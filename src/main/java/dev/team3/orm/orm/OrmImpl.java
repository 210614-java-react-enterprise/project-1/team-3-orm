package dev.team3.orm.orm;

import dev.team3.orm.annotations.*;
import dev.team3.orm.util.ConnectionUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrmImpl {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static boolean delete(Class<?> clazz, int id, String column){
        Table table = clazz.getAnnotation(Table.class);
        String sql = "delete from " + table.name() + " where " + column + " = ?";

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setObject(1, id);
            int value = ps.executeUpdate();
            if(value > 0){
                return true;
            }

        }catch (SQLException throwables){
            throwables.printStackTrace();
        }
        return false;
    }

    public static List<Object> getAll(Class<?> clazz){
        Object newObject = null;
        Field[] fields = clazz.getDeclaredFields();
        List<Field> fields1 = new ArrayList<>();
        for(Field f: fields){
            FieldIgnore fi = f.getAnnotation(FieldIgnore.class);
            if(fi == null){
                fields1.add(f);
            }
        }
        List<String> cArr = new ArrayList<>();
        for(Field f: fields1){
            String columnName = "";
            ObjectKey objectKey = f.getAnnotation(ObjectKey.class);
            if(objectKey != null){
                columnName = objectKey.name();
                cArr.add(columnName);
            }
        }

        Table table = clazz.getAnnotation(Table.class);
        String sql = "select * from " + table.name();
        try(Connection connection = ConnectionUtil.getConnection();
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(sql)){
            List<Object> allGames = new ArrayList<>();
            while(rs.next()){
                newObject = clazz.getDeclaredConstructor().newInstance();
                for(int i = 0; i < fields1.size(); i++){
                    String setterName = "set"+fields1.get(i).getName().substring(0,1).toUpperCase() + fields1.get(i).getName().substring(1);
                    Method setterMethod = clazz.getMethod(setterName, fields1.get(i).getType());
                    String type = fields1.get(i).getType().toString();
                    switch (type){
                        case "int":
                            setterMethod.invoke(newObject, rs.getInt(cArr.get(i)));
                            break;
                        case "class java.lang.String":
                            setterMethod.invoke(newObject, rs.getString(cArr.get(i)));
                            break;
                        case "boolean":
                            setterMethod.invoke(newObject, rs.getBoolean(cArr.get(i)));
                            break;
                        case "long":
                            setterMethod.invoke(newObject, rs.getLong(cArr.get(i)));
                            break;
                        case "java.util.Array":
                            setterMethod.invoke(newObject, rs.getArray(cArr.get(i)));
                            break;
                        case "short":
                            setterMethod.invoke(newObject, rs.getShort(cArr.get(i)));
                            break;
                        case "byte":
                            setterMethod.invoke(newObject, rs.getByte(cArr.get(i)));
                            break;
                        case "java.math.BigDecimal":
                            setterMethod.invoke(newObject, rs.getBigDecimal(cArr.get(i)));
                            break;
                        case "java.util.Date":
                            setterMethod.invoke(newObject, rs.getDate(cArr.get(i)));
                            break;
                        default:
                            break;
                    }
                }
                allGames.add(newObject);
            }
            return allGames;

        }catch (SQLException | NoSuchMethodException throwables){
            throwables.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <E> E create(E dbRecord)
    {
        //TODO: Instead of explicitly ignoring ID annotations manually, consider adding a new annotation type that blacklists them?
        //TODO: Remove the printed stack traces from the exceptions in here and just perform SOUT operations that note the type of violation
        //TODO: May need to update database so that game_id and user_id in the reviews tables are indeed foreign keys, rather than just NOT NULL
        try(Connection connection = ConnectionUtil.getConnection())
        {
            //Dynamically gets the table name to use for the object via its annotation
            String tableName = pullFieldName(dbRecord.getClass());

            //This is used to dynamically determine the number of parameters that will be required to send off to the database
            //to insert a new entry
            String prepSqlParams = "";

            //Get the declared annotated fields of the object
            Field[] fields = dbRecord.getClass().getDeclaredFields();
            ArrayList<ObjectKey> objAnnotations = new ArrayList<>();
            String columns = "";
            ArrayList<Type> recordColumnTypes = new ArrayList<>();
            ArrayList<String> recordColumnValues = new ArrayList<>();

            ArrayList<Method> fieldGetters = new ArrayList<>();

            for (Field thefield:fields)
            {
                //Skip the field if it is one that is generated by the DATABASAE, not by the ORM
                if(thefield.isAnnotationPresent(IsDbGenerated.class))
                {
                    continue;
                }

                //Each iteration gets EVERY annotation attached to the field it's currently checking
                ObjectKey[] theFieldAnnos = thefield.getAnnotationsByType(ObjectKey.class);
                for (ObjectKey anno:theFieldAnnos)
                {
                    //TODO: Add check to make sure we're only adding annotations of type ObjectKey
                    //TODO: Add functionality to filter out anything that will be created specifically by the SQLDatabase, like SERIAL values
                    if (anno.name().equals("admin"))
                        continue;

                    prepSqlParams += "?,";
                    objAnnotations.add(anno);
                    columns += anno.name() + ",";

                    //TODO: Implement a name-converter method to do the String conversion for method-finding
                    //Get the method to call for this particular field
                    String getterString = "get" + anno.name().substring(0,1).toUpperCase() + anno.name().substring(1);
                    try
                    {
                        while(getterString.contains("_"))
                        {
                            int underscoreIndex = getterString.indexOf("_");

                            getterString = getterString.substring(0,underscoreIndex) + getterString.substring(underscoreIndex+1,underscoreIndex+2).toUpperCase() + getterString.substring(underscoreIndex+2);
                            System.out.println("GETTER STRING IS " + getterString);
                        }

                        //Add the field value to the list of values to send off to the database
                        Method getterMethod = dbRecord.getClass().getMethod(getterString);
                        String fieldValue = getterMethod.invoke(dbRecord).toString();

                        //////////////////////////////////////////
                        //Add the getter method to the list of field getters
                        fieldGetters.add(getterMethod);
                        System.out.println("Adding this method to the fieldGetters list: " + getterMethod.getName());
                        //////////////////////////////////////////

                        //recordColumnValues.add(fieldValue);
                    }
                    catch (NoSuchMethodException e)
                    {
                        e.printStackTrace();
                        System.out.println("No such getter exists for this field: " + anno.name().toString());
                    } catch (InvocationTargetException | IllegalAccessException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            //Trim last comma out of preSqlParams and columns
            prepSqlParams = prepSqlParams.substring(0,prepSqlParams.lastIndexOf(','));
            columns = columns.substring(0,columns.lastIndexOf(','));

            //Prepare the basic string format of the SQL
            String sql = "insert into " + tableName + " (" + columns + ") values (" + prepSqlParams + ")";
            System.out.println("SQL is currently " + sql);

            //Prepare the actual statement
            PreparedStatement insertStatement = connection.prepareStatement(sql);
            //TODO: typechecking, so that the method dosen't insert everything just as a string...
            //TODO: Check if there could be any issues with the insertStatement having single quotes in there, surrounding the column identifiers
            for (int x=0; x<objAnnotations.size(); x++)
            {
                System.out.println("Scanning current ObjectKey. Key name is:" + objAnnotations.get(x).name());
                //String arg = recordColumnValues.get(x);

                /////////////////////////////////////////////////////////
                Class<?> currentVarType = fieldGetters.get(x).getReturnType();

                System.out.println("Current type is: " + currentVarType.getSimpleName());
                //Dynamically set the preparedStatement arguments
                try
                {
                    switch (currentVarType.getSimpleName())
                    {
                        case "String" :
                            insertStatement.setString(x+1,(String)fieldGetters.get(x).invoke(dbRecord));
                            break;
                        case "int":
                            insertStatement.setInt(x+1,(int)fieldGetters.get(x).invoke(dbRecord));
                    }

                } catch (IllegalAccessException | InvocationTargetException e)
                {
                    e.printStackTrace();
                }
                /////////////////////////////////////////////////////////
                //insertStatement.setString(x+1,arg);
            }
            System.out.println("InsertStatement is currently " + insertStatement.toString());

            //Attempt to execute
            //TODO: Figure out if we should just use "execute()" instead of executeUpdate()
            int success = insertStatement.executeUpdate();
            if (success>0)
            {
                System.out.println("Success!  New database entry added successfully!");
                return dbRecord;
            } else {
                return null;
            }

        } catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Failed to connect to database server.");
        }
        //TODO: Remove this null return value
        return null;
    }

    public static <E> E update(E dbRecord)
    {
        //TODO: Instead of explicitly ignoring ID annotations manually, consider adding a new annotation type that blacklists them?
        //TODO: Remove the printed stack traces from the exceptions in here and just perform SOUT operations that note the type of violation
        //TODO: May need to update database so that game_id and user_id in the reviews tables are indeed foreign keys, rather than just NOT NULL
        try(Connection connection = ConnectionUtil.getConnection())
        {
            //Dynamically gets the table name to use for the object via its annotation
            String tableName = pullFieldName(dbRecord.getClass());

            //This is used to dynamically determine the number of parameters that will be required to send off to the database
            //to insert a new entry
            String prepSqlParams = "";

            //Get the declared annotated fields of the object
            Field[] fields = dbRecord.getClass().getDeclaredFields();
            ArrayList<ObjectKey> objAnnotations = new ArrayList<>();
            ArrayList<String> columns = new ArrayList<>();
            ArrayList<String> primaryKeyColumns = new ArrayList<>();
            ArrayList<Type> recordColumnTypes = new ArrayList<>();
            ArrayList<String> recordColumnValues = new ArrayList<>();

            ArrayList<Method> fieldGetters = new ArrayList<>();
            ArrayList<Method> pkeyGetters = new ArrayList<>();



            for (Field thefield:fields)
            {
                if (thefield.isAnnotationPresent(PrimaryKey.class))
                {
                    primaryKeyColumns.add(thefield.getName());
                    System.out.println("FIELD NAME IS " + thefield.getName());

                    //Save getter to arrayList
                    String pkeyGetter = thefield.getName();
                    pkeyGetter = pkeyGetter.substring(0,1).toUpperCase() + pkeyGetter.substring(1);
                    pkeyGetter = "get" + pkeyGetter;

                    System.out.println("pkeyGetter String value is " + pkeyGetter);

                    try
                    {
                        pkeyGetters.add(dbRecord.getClass().getMethod(pkeyGetter));
                    } catch (NoSuchMethodException e)
                    {
                        e.printStackTrace();
                        System.out.println("Could not find method " + pkeyGetter);
                    }

                    //TODO: Logic to handle adding the Primary Key element, for the sake of the WHERE clause
                }

                //Skip further operations if the field if it is one that is generated by the DATABASAE, not by the ORM
                if(thefield.isAnnotationPresent(IsDbGenerated.class))
                {
                    continue;
                }

                //Each iteration gets EVERY annotation attached to the field it's currently checking
                ObjectKey[] theFieldAnnos = thefield.getAnnotationsByType(ObjectKey.class);
                for (ObjectKey anno:theFieldAnnos)
                {
                    //TODO: Add check to make sure we're only adding annotations of type ObjectKey
                    //TODO: Add functionality to filter out anything that will be created specifically by the SQLDatabase, like SERIAL values
                    if (anno.name().equals("admin"))
                        continue;

                    prepSqlParams += "?,";
                    objAnnotations.add(anno);
                    columns.add(anno.name());

                    //TODO: Implement a name-converter method to do the String conversion for method-finding
                    //Get the method to call for this particular field
                    String getterString = "get" + anno.name().substring(0,1).toUpperCase() + anno.name().substring(1);
                    try
                    {
                        while(getterString.contains("_"))
                        {
                            int underscoreIndex = getterString.indexOf("_");

                            getterString = getterString.substring(0,underscoreIndex) + getterString.substring(underscoreIndex+1,underscoreIndex+2).toUpperCase() + getterString.substring(underscoreIndex+2);
                            System.out.println("GETTER STRING IS " + getterString);
                        }

                        //Add the field value to the list of values to send off to the database
                        Method getterMethod = dbRecord.getClass().getMethod(getterString);
                        String fieldValue = getterMethod.invoke(dbRecord).toString();

                        //////////////////////////////////////////
                        //Add the getter method to the list of field getters
                        fieldGetters.add(getterMethod);
                        //System.out.println("Adding this method to the fieldGetters list: " + getterMethod.getName());
                        //////////////////////////////////////////

                        //recordColumnValues.add(fieldValue);
                    }
                    catch (NoSuchMethodException e)
                    {
                        e.printStackTrace();
                        System.out.println("No such getter exists for this field: " + anno.name().toString());
                    } catch (InvocationTargetException | IllegalAccessException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            //Trim last comma out of preSqlParams
            prepSqlParams = prepSqlParams.substring(0,prepSqlParams.lastIndexOf(','));

            //Prepare a string to represent the SET part of the SQL statement
            String prepColumnNames = "";
            for (String str:columns)
            {
                prepColumnNames += str + " = ?, ";
            }
            //System.out.println("PrepColumnNames is currently " + prepColumnNames);
            //Trim last comma off of prepColumnNames
            prepColumnNames = prepColumnNames.substring(0,prepColumnNames.lastIndexOf(','));

            String prepPkeySql = "";
            for (String pkey:primaryKeyColumns)
            {
                System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                System.out.println("pkey here is " + pkey);

                String pkeyToSqlSyntax = pkey;
                boolean hasUppercase = false;

                for (int y=0; y<pkey.length();y++)
                {
                    //If any characters are uppercase, flag for alteration
                    if(Character.isUpperCase(pkey.charAt(y)))
                    {
                        System.out.println("Found an uppercase character");
                        hasUppercase = true;
                    }
                }

                //If flag was set
                if(hasUppercase)
                {
                    System.out.println("Flag was set; converting to SQL syntax");
                    //pkeyToSqlSyntax = "";
                    //Convert to proper conventional SQL underscore syntax
                    for (int y=0; y<pkey.length(); y++)
                    {
                        //If uppercase,
                        if(Character.isUpperCase(pkey.charAt(y)))
                        {
                            //Update proper syntax
                            System.out.println("Prior to syntax update: " + pkeyToSqlSyntax);
                            pkeyToSqlSyntax = pkeyToSqlSyntax.substring(0,y) + "_" + pkeyToSqlSyntax.substring(y);
                            System.out.println("Syntax update: " + pkeyToSqlSyntax);
                            y++;
                        }
                    }
                    pkeyToSqlSyntax = pkeyToSqlSyntax.toLowerCase();
                }

                System.out.println("pkeyToSqlSyntax is " + pkeyToSqlSyntax);
                prepPkeySql += pkeyToSqlSyntax + " = ? AND ";
            }
            //Trim last comma off of prepPkeySql
            prepPkeySql = prepPkeySql.substring(0,prepPkeySql.lastIndexOf("AND"));
            //System.out.println("PREPPKEYSQL is currently " + prepPkeySql);

            //Prepare the basic string format of the SQL
            String sql = "update " + tableName + " set " + prepColumnNames + " where " + prepPkeySql;
            System.out.println("SQL is currently " + sql);

            //Prepare the actual statement
            PreparedStatement insertStatement = connection.prepareStatement(sql);
            //TODO: typechecking, so that the method dosen't insert everything just as a string...
            //TODO: Check if there could be any issues with the insertStatement having single quotes in there, surrounding the column identifiers
            for (int x=0; x<objAnnotations.size(); x++)
            {
                System.out.println("Scanning current ObjectKey. Key name is:" + objAnnotations.get(x).name());
                //String arg = recordColumnValues.get(x);

                /////////////////////////////////////////////////////////
                Class<?> currentVarType = fieldGetters.get(x).getReturnType();

                System.out.println("Current type is: " + currentVarType.getSimpleName());
                //Dynamically set the preparedStatement arguments
                try
                {
                    switch (currentVarType.getSimpleName())
                    {
                        case "String" :
                            insertStatement.setString(x+1,(String)fieldGetters.get(x).invoke(dbRecord));
                            break;
                        case "int":
                            insertStatement.setInt(x+1,(int)fieldGetters.get(x).invoke(dbRecord));

                            break;
                    }
                    System.out.println("SQL statement is now: " + insertStatement.toString());

                } catch (IllegalAccessException | InvocationTargetException e)
                {
                    e.printStackTrace();
                }
                /////////////////////////////////////////////////////////
                //insertStatement.setString(x+1,arg);
            }

            System.out.println("====================================");
            System.out.println("====================================");
            System.out.println("====================================");
            System.out.println("====================================");
            //Set up WHERE sql parameters

            int zOffset = objAnnotations.size();
            System.out.println("INSERT STATEMENT: " + insertStatement.toString());
            for (int z =0; z<primaryKeyColumns.size(); z++)
            {
                System.out.println("pkey in pkey columns is " + primaryKeyColumns.get(z));
                System.out.println("pkeygetter return value is " + pkeyGetters.get(z).getReturnType().getSimpleName());

                try
                {
                    String type = pkeyGetters.get(z).getReturnType().getSimpleName();
                    System.out.println("type is currently " + type);

                    switch (type)
                    {
                        case "String":
                            System.out.println("Attempting to update string value...");
                            System.out.println("Setting STRING at " + (z+zOffset+1) + "to" + (String)pkeyGetters.get(z).invoke(dbRecord));
                            insertStatement.setString(z+zOffset+1,(String)pkeyGetters.get(z).invoke(dbRecord));
                            break;
                        case "int":
                            System.out.println("Attempting to update int value...");
                            System.out.println("Setting INT at " + (z+zOffset+1) + "to" + (int)pkeyGetters.get(z).invoke(dbRecord));
                            insertStatement.setInt(z+zOffset+1,(int)pkeyGetters.get(z).invoke(dbRecord));
                            break;
                    }
                } catch (IllegalAccessException | InvocationTargetException e)
                {
                    e.printStackTrace();
                }
            }

            System.out.println("InsertStatement is currently " + insertStatement.toString());

            //Attempt to execute
            int success = insertStatement.executeUpdate();
            if (success>0)
            {
                System.out.println("Success!  New database entry added successfully!");
            }
            return null;
        } catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Failed to connect to database server.");
        }
        //TODO: Remove this null return value
        return null;
    }

    //TODO: Add some descriptive commentary to describe what this actually does
    private static String pullFieldName(Class<?> clazz)
    {
        Annotation[] classAnnos = clazz.getAnnotations();

        for (Annotation anno:classAnnos)
        {
            System.out.println("Class annotation is: " + anno.toString());
            if (anno instanceof Table)
            {
                return ((Table) anno).name();
            }
        }
        throw new IllegalArgumentException();
    }

    public int authenticateUser(String username, String password) {
        int access = 0;
        String sql = "select * from users where username = (?) AND password = (?)";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);){

            ps.setString(1,username);
            ps.setString(2, password);
            ResultSet search = ps.executeQuery();
            if(search.next()){
                if(search.getBoolean("admin")==true){
                    return 2;
                } else {
                    return 1;
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            System.out.println("Failed to verify user.");
        }
        return 0;
    }
}
