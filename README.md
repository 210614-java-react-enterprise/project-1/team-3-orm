# Project 1 - Custom Object Relational Mapping Framework
## Link to Web App https://gitlab.com/210614-java-react-enterprise/project-1/team-3-webapp/
# Team-3 (Greg McCoy, Cody Anderson, and Conor Scalf)
## Description

Our first project was to create a custom object relational mapping (ORM) framework. This framework allowed for a simplified and SQL-free interaction with the relational data source. Once the ORM is created we can import the ORM and use our Web App to communicate with database through the ORM. Our team decided to create a simple Web App where users can: create reviews for games, update reviews for games, and delete reviews. Our vision for this App was to have a place where gamers can feel free to express their feelings for a game. There were many ways that this task could have been approached and after doing a lot of research our team came up with an approach.

## Tech Stack

- [ ] Java 8
- [ ] JUnit
- [ ] Mockito
- [ ] Apache Maven
- [ ] Jackson library (for JSON marshalling/unmarshalling)
- [ ] Java EE Servlet API (v4.0+)
- [ ] PostGreSQL deployed on AWS RDS
- [ ] Git SCM (on GitLab)

## ORM CRUD Functions

- [ ] getAll(Class<?> c): The getAll() method takes a generic class and returns all of the records from a table after creating objects using the given class.
- [ ] delete(Class<?> c, int id, String column): The delete() method takes a class, an id, and a column and deletes the existing record with those paramters.
- [ ] create(E dbRecord): The create() method takes an object and creates a record in the database.
- [ ] update(E dbRecord): The update() method takes an object and updates the record that holds that object in the database.

## ORM Annotations

- [ ] @FieldIgnore: This annotation is used to ignore certain fields.
- [ ] @ObjectKey: This annotaion is used to find columns in a database.
- [ ] @PrimaryKey: This annotaion is used to identify primary keys in a model.
- [ ] @Table: This annotation is used to specify the table that pertains to a model.
- [ ] @IsDbGenerated: This annotation is used to indicate that a value is generated by the database.

## Web App Servlets

- [ ] LoginServlet: Accepts a post request and validates user, then creates user session.
- [ ] RegisterServlet: Accepts a post request, creates a new user, and then logs user in.
- [ ] ReviewServlet: Checks for a user session and accepts get, put, post, and delete requests.
- [ ] UserServlet: Checks for user session and accepts get, put, and delete requests.
- [ ] GameServlet: Checks for a user session and accepts get, post, and delete requests.

## Web App Services

- [ ] AuthService: Checks for a users authorization in the Web App.
- [ ] GameService: Communicates with the ORM to get, create, and delete game objects. Uses streams to return a specific search from requests.
- [ ] ReviewService: Communicates with the ORM to get, create, update and delete review objects. Uses streams to return a specific search from requests.
- [ ] UserService: Communicates with the ORM to get, create, update and delete user objects. Uses streams to return a specific search from requests.
